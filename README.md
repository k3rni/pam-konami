# pam_konami.so

This is a PAM authentication module that accepts the classic Konami code as a valid password. It was inspired by [wumb0's version](https://github.com/wumb0/pam_konami), but I wanted to make one that requires an actual gamepad for entry.

Written in Rust, so the finished binary is quite larger than other PAM modules.

# Why?

Just for fun! And to learn Rust.

# Usage

Only Linux for now, should be portable as long as `libevdev` works on your system and it uses PAM.

First, build the project with `cargo build --release`. Then run `sudo make install` to install the PAM module.

Next, you need to manually update one of your PAM rule files, which are located in `/etc/pam.d`. I strongly discourage modifying `system-auth`, `common-auth` or `login`, so as not to lock yourself out of the system, should anything fail badly. A safe choice is `sudo`. It will look something like this:


    #%PAM-1.0
    auth		include		system-auth
    account		include		system-auth
    session		include		system-auth

Add `auth sufficient pam_konami.so` before the first `auth` line. 

Open a new terminal, and invoke sudo. If you have no gamepads connected (or none that are recognized), you should get the password prompt almost immediately. Not much changes there.

However, if you have a gamepad connected, there will be a 10s pause before you receive the password prompt. This is your time to grab the gamepad and enter the classic Konami code: _Up, Up, Down, Down, Left, Right, Left, Right, B, A_. On completion, you will be granted whatever priviledges you configured the module for; in this case root access **without** being prompted for a password (this is what the `sufficient` part means).

If there is more than one gamepad detected: the *last* one will be used. Device enumeration order usually matches plugging-in order, so this will likely be the most-recently-attached gamepad.

# Entering the code

The movements can be entered on either the first analog stick or the D-pad, if it's detected as a directional hat. If there's no analog stick, the D-pad works as well.

For buttons, the near-standard arrangement is, regardless of the actual button labels:

       [Y]                           [North]
    [X]   [B] or, more generally [West]   [East]
       [A]                           [South]

Your controller may have some of these swapped, so try a couple of times to see what works. A special provision has been made for Logitech Rumblepad, which uses entirely different button codes. Other gamepads I've tested, including a wireless version of that Rumblepad, use a common set.

## Konami Code Purists

The full classic code ends with <kbd>Start</kbd>. However, many gamepads either do not have this button, or have a different button placed roughly where you'd expect Start to be. However, I'm not guessing which one it is: is it button 7? 8? 10? By being a bit generic, a larger range of gamepads can be used.


# Only for local use

For obvious reasons, this is not usable on remote hosts. But [wumb0's version](https://github.com/wumb0/pam_konami) may be, as it only needs keyboard input.
