PAM_MODULES_DIR = $(shell find /lib/ -maxdepth 2 -type d -name security)

all:

pam_konami.so: target/release/libkonami.so
	strip -o $@ $^

install: pam_konami.so
	install $^ $(PAM_MODULES_DIR) -o root -g root
	rm -f $^

target/release/%.so: 
	@echo "Run 'cargo build --release' first. Then rerun 'make install' with sudo."
	@false
