use std::fs::File;
use std::path::Path;

use evdev_rs::enums::{EventCode, EventType, EV_ABS, EV_KEY, EV_REL};
use evdev_rs::Device;

trait CapabilityDetection {
    fn has_xy(&self) -> bool;
    fn has_hat_xy(&self) -> bool;
    fn has_btn_se(&self) -> bool;
    fn has_btn_thumb(&self) -> bool;
    fn has_wheel(&self) -> bool;
}

impl CapabilityDetection for Device {
    #[inline]
    fn has_wheel(&self) -> bool {
        // Likely a mouse
        self.has(&EventType::EV_REL) && self.has(&EventCode::EV_REL(EV_REL::REL_WHEEL))
    }

    #[inline]
    fn has_xy(&self) -> bool {
        self.has(&EventCode::EV_ABS(EV_ABS::ABS_X)) && self.has(&EventCode::EV_ABS(EV_ABS::ABS_Y))
    }

    #[inline]
    fn has_hat_xy(&self) -> bool {
        // D-pad on some gamepads reports as hat0
        self.has(&EventCode::EV_ABS(EV_ABS::ABS_HAT0X))
            && self.has(&EventCode::EV_ABS(EV_ABS::ABS_HAT0Y))
    }

    #[inline]
    fn has_btn_se(&self) -> bool {
        self.has(&EventCode::EV_KEY(EV_KEY::BTN_SOUTH))
            && self.has(&EventCode::EV_KEY(EV_KEY::BTN_EAST))
    }

    #[inline]
    fn has_btn_thumb(&self) -> bool {
        // Logitech Rumblepad 2. But its Cordless variant has SOUTH and EAST instead.
        self.has(&EventCode::EV_KEY(EV_KEY::BTN_THUMB))
            && self.has(&EventCode::EV_KEY(EV_KEY::BTN_THUMB2))
    }
}

fn check_device(path: &Path) -> Result<bool, ()> {
    let fd = File::open(path).map_err(|_| ())?;
    let dev = Device::new_from_fd(fd).map_err(|_| ())?;

    Ok(!dev.has_wheel()
        && (dev.has_btn_thumb() || dev.has_btn_se())
        && (dev.has_xy() || dev.has_hat_xy()))
}

pub fn is_gamepad(path: &Path) -> bool {
    check_device(path).unwrap_or(false)
}
