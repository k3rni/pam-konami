#![allow(clippy::not_unsafe_ptr_arg_deref)]
#[macro_use]
extern crate pamsm;

pub mod gamepad;
pub mod recognizer;

use pamsm::{Pam, PamError, PamFlag, PamServiceModule};

use gamepad::is_gamepad;
use glob::glob;
use recognizer::{CodeRecognizer, CodeUnit};

struct KonamiAuth;

impl PamServiceModule for KonamiAuth {
    #[allow(unused_variables)]
    fn authenticate(self: &Self, pamh: Pam, _: PamFlag, args: Vec<String>) -> PamError {
        let evdir = match glob("/dev/input/event*") {
            Ok(names) => names,
            Err(e) => {
                return PamError::AUTHINFO_UNAVAIL;
            }
        };
        let target_device = evdir
            .filter(|name| name.as_ref().map(|path| is_gamepad(path)).unwrap_or(false))
            .map(|name| name.unwrap())
            .last();

        match target_device {
            Some(path) => {
                use CodeUnit::*;
                let mut crg = CodeRecognizer::new(
                    &path,
                    // TODO: default to Konami, but accept code from args
                    &[Up, Up, Down, Down, Left, Right, Left, Right, B, A],
                );
                if crg.run().is_ok() {
                    PamError::SUCCESS
                } else {
                    PamError::AUTHINFO_UNAVAIL
                }
            }
            None => PamError::IGNORE,
        }
    }
}

pamsm_init!(Box::new(KonamiAuth));
