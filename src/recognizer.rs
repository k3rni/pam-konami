use evdev_rs::enums::{EventCode, EventType, EV_ABS, EV_KEY};
use evdev_rs::{AbsInfo, Device, InputEvent, ReadFlag, ReadStatus};
use nix::errno::Errno;
use nix::fcntl::{fcntl, OFlag, F_SETFL};
use nix::sys::select::{select, FdSet};
use nix::sys::time::{TimeVal, TimeValLike};
use std::convert::TryFrom;
use std::fs::File;
use std::path::Path;
use std::time::{Duration, Instant};

#[cfg(unix)]
use std::os::unix::io::AsRawFd;

pub struct CodeRecognizer<'lifetime> {
    dev_path: &'lifetime Path,
    sequence: &'lifetime [CodeUnit],
    timeout: Duration,
    progress: usize,
    xprev: Option<f32>,
    yprev: Option<f32>,
}

#[derive(Debug, PartialEq)]
pub enum CodeUnit {
    Up,
    Down,
    Left,
    Right,
    A,
    B,
}

impl TryFrom<char> for CodeUnit {
    type Error = ();

    fn try_from(value: char) -> Result<Self, Self::Error> {
        use CodeUnit::*;
        match value {
            'U' => Ok(Up),
            'D' => Ok(Down),
            'L' => Ok(Left),
            'R' => Ok(Right),
            'A' => Ok(A),
            'B' => Ok(B),
            _ => Err(()),
        }
    }
}

pub fn code_from_str(code: &str) -> Vec<CodeUnit> {
    code.chars()
        .filter_map(|ch| CodeUnit::try_from(ch).ok())
        .collect()
}

trait DeviceWithAbs {
    fn get_abs_info(&self, code: &EventCode) -> Option<AbsInfo>;
}

impl DeviceWithAbs for Device {
    fn get_abs_info(&self, code: &EventCode) -> Option<AbsInfo> {
        self.abs_info(code)
    }
}

impl<'lifetime> CodeRecognizer<'lifetime> {
    pub fn new(path: &'lifetime Path, sequence: &'lifetime [CodeUnit]) -> Self {
        Self {
            dev_path: path,
            sequence,
            timeout: Duration::from_secs(10),
            progress: 0,
            xprev: None,
            yprev: None,
        }
    }

    pub fn run(&mut self) -> Result<(), ()> {
        let fd = File::open(self.dev_path).map_err(|_| ())?;
        let rawfd = fd.as_raw_fd();
        fcntl(rawfd, F_SETFL(OFlag::O_NONBLOCK)).unwrap_or_else(|e| panic!(e));
        let dev = Device::new_from_fd(fd).map_err(|_| ())?;

        let start = Instant::now();
        let mut fds = FdSet::new();
        while let Some(left) = self
            .timeout
            .checked_sub(Instant::now().duration_since(start))
        {
            let mut select_timeout = TimeVal::milliseconds(left.as_millis() as i64);
            fds.insert(rawfd);
            let ready = select(None, Some(&mut fds), None, None, Some(&mut select_timeout))
                .unwrap_or_else(|e| panic!(e));
            if ready > 0 && self.process_events(&dev) {
                return Ok(());
            }
        }
        // Time expired, code not entered
        Err(())
    }

    fn process_events(&mut self, dev: &Device) -> bool {
        loop {
            match dev.next_event(ReadFlag::NORMAL) {
                Err(Errno::EAGAIN) => return self.code_entered(),
                Ok((ReadStatus::Success, event)) | Ok((ReadStatus::Sync, event)) => {
                    self.process_event(&event, dev)
                }
                Err(e) => panic!(e),
            }
        }
    }

    fn process_event(&mut self, event: &InputEvent, dev: &Device) {
        use EventType::*;
        match event {
            InputEvent {
                event_type: EV_ABS, ..
            } => self.process_abs(event, dev.abs_info(&event.event_code)),
            InputEvent {
                event_type: EV_KEY, ..
            } => self.process_key(event),
            InputEvent {
                event_type: EV_SYN, ..
            } => (), // Not sure why, but behavior is slightly different if this is not matched explicitly.
            _ => (),
        }
    }

    fn process_abs(&mut self, event: &InputEvent, absinfo: Option<AbsInfo>) {
        let absinfo = match absinfo {
            Some(absinfo) => absinfo,
            None => return, // Impossible: to report an abs event and have no absinfo data
        };
        use CodeUnit::*;

        let range = (absinfo.maximum - absinfo.minimum) as f32;
        let center = (absinfo.minimum + absinfo.maximum) / 2;
        let lo = absinfo.minimum as f32 + range / 5.0;
        let hi = absinfo.maximum as f32 - range / 5.0;

        match event.event_code {
            EventCode::EV_ABS(EV_ABS::ABS_X) | EventCode::EV_ABS(EV_ABS::ABS_HAT0X) => {
                let x = event.value as f32;
                // println!("X({:?} => {}) [{} {}] {}:{}:{}", self.xprev, x, absinfo.minimum, absinfo.maximum, lo, center, hi);
                let prev = self.xprev.unwrap_or(center as f32);
                if x >= hi && prev < hi {
                    self.emit(Right);
                } else if x <= lo && prev > lo {
                    self.emit(Left);
                }
                self.xprev = Some(x);
            }
            EventCode::EV_ABS(EV_ABS::ABS_Y) | EventCode::EV_ABS(EV_ABS::ABS_HAT0Y) => {
                let y = event.value as f32;
                // println!("Y({:?} => {}) [{} {}] {}:{}:{}", self.yprev, y, absinfo.minimum, absinfo.maximum, lo, center, hi);
                let prev = self.yprev.unwrap_or(center as f32);
                if y >= hi && prev < hi {
                    self.emit(Down);
                } else if y <= lo && prev > lo {
                    self.emit(Up);
                }
                self.yprev = Some(y);
            }
            _ => (),
        }
    }

    fn process_key(&mut self, event: &InputEvent) {
        if event.value != 0 {
            return;
        }; // Only handle button releases
        use CodeUnit::*;
        match event.event_code {
            EventCode::EV_KEY(EV_KEY::BTN_SOUTH) | EventCode::EV_KEY(EV_KEY::BTN_THUMB) => {
                self.emit(A)
            }
            EventCode::EV_KEY(EV_KEY::BTN_EAST) | EventCode::EV_KEY(EV_KEY::BTN_THUMB2) => {
                self.emit(B)
            }
            _ => (),
        }
    }

    fn code_entered(&self) -> bool {
        self.progress >= self.sequence.len()
    }

    fn emit(&mut self, code: CodeUnit) {
        if self.code_entered() {
            return;
        }
        if self.sequence[self.progress] != code {
            self.progress = 0;
            return;
        }
        self.progress += 1;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_code_from_str() {
        use CodeUnit::*;
        assert_eq!(code_from_str("RDA"), &[Right, Down, A]);
        assert_eq!(code_from_str(":XD"), &[Down]);
        assert_eq!(code_from_str("WTF"), &[]);
    }
}
