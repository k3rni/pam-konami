extern crate konami;

use glob::glob;
use konami::gamepad::is_gamepad;
use konami::recognizer::{CodeRecognizer, CodeUnit};

fn main() {
    let evdir = match glob("/dev/input/event*") {
        Ok(names) => names,
        Err(e) => {
            println!("{:?}", e);
            return;
        }
    };

    let target_device = evdir
        .filter(|name| name.as_ref().map(|path| is_gamepad(path)).unwrap_or(false))
        .map(|name| name.unwrap())
        .last();

    if let Some(path) = target_device {
        println!("A gamepad found at {}", &path.to_str().unwrap());
        use CodeUnit::*;
        let mut crg =
            CodeRecognizer::new(&path, &[Up, Up, Down, Down, Left, Right, Left, Right, B, A]);
        if crg.run().is_ok() {
            println!("KONAMI CODE ENTERED!");
        }
    }
}
